#!/usr/bin/perl

sub encryptString {
  $rawStr = $_[0];
  $encryptedStr = "";
  $numChars = length($rawStr);
  for ($i = 0; $i < $numChars; $i++) {
    $encryptedStr .= chr(ord(substr($rawStr,$i,1)) + (80 - $i) );
  }
  return $encryptedStr;
}

sub formatStr {
  $strName = $_[0];
  $str = $_[1];
  $numChars = length($str);
  $numChars2 = $numChars+1;
  $formattedStr = "static unsigned char ${strName}[$numChars2] = {\n";
  # We output the string in groups of 6 characters
  $chunkLength = 10;
  $numCharsInChunk = 0;
  for ($i = 0; $i < $numChars; $i++) {
    $formattedStr .= sprintf("  0x%02x, ",ord(substr($str,$i,1)));
    if ($numCharsInChunk++ == $chunkLength) {
      $formattedStr .= "\n";
      $numCharsInChunk = 0;
    }
  }
  if ($numCharsInChunk != 0 && $numCharsInChunk < $chunkLength) {
    $formattedStr .= "\n";
  }
  $formattedStr .= "  0x00\n";
  $formattedStr .= "};\n";
  return $formattedStr;  
}

$strName = $ARGV[0];
$str = $ARGV[1];

$programName = $0;
print "// Original string: $str\n";
print "// Encrypted string generated with $programName\n";
print formatStr($strName, encryptString($str)) . "\n";

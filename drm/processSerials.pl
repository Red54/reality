#!/usr/bin/perl

# This script processes the serial numbers to format them for the Reality 2.0 specification
#
# The expected input is a series of number generated with uuidgen (Mac OS) 
# To flag them as Reality 2.0 number they are prefixed with "R2DS" (Reality 2 for daz Studio)
# To hide that they are Generation 4 GUIDs the 12 character, which is always 4, is replaced
# with a randomly chosen character from the word REALITY. 
#
# Finally, the 17th character, one of (A, B, 8, 9), is replaced with a substitution table.
#
# Copyright (c) 2011 Pret-a-3D/Paolo Ciccone. All rights reserved.
#
#

use strict;
use warnings;

my $num_args = $#ARGV + 1;
if ($num_args < 1) {
  print "\nYou must pass the name of the file containing the serial number, dumbass!\n";
  exit;
}
my $vendorCode = "P";  # P=Pret-a-3D, D=DAZ, R=Renderosity
if ($num_args == 2 ) {
  $vendorCode = substr($ARGV[1],0,1);
}

#my $serials = "reality_2.0.serials.txt";
my $serials = $ARGV[0];
my $prefix  = "R2DS";
open(my $fh, $serials) or die "Error: cannot open file $serials for reading\n";

my @replacement1 = ('R','E','A','L','I','T','Y');
my $numReplacements = $#replacement1;
# Replacement tabl for the 17th characters. Another way to obfuscate the 4G GUID
my %seventeenChar = ('A' => 'P', 'B' => 'A', '8' => '3', '9' => 'D');

my $rowNo = 0;
while(<$fh>) {
  # Remove the dashes from the input line
  s/-//g;
  my $line = $_;
  $rowNo++;
  chomp($line);
  # The first 12 characters are OK. The 13th, constant 4, is replaced to hide that this
  # is a th generation GUID
  my $head = substr($line,0,12);
  # Pick a replacement randomly
  my $fourReplaced = $replacement1[rand($numReplacements)];
#  print ">>", substr($line,17,1) , "\n";
  my $seventeenReplaced = $seventeenChar{substr($line,16,1)};
  my $coda1 = substr($line,13,3);
  my $coda2 = substr($line,17);

  # CSV output
  # productCode, serialNumber, seller, used
  # Format of the serial code:
  #   Product prefix: R2DS  (Reality 2 for Daz Studio)
  #   Vendor code
  #   GUID with replaced characters as rules above
  print "\"RE2DS\",\"${prefix}${vendorCode}${head}${fourReplaced}${coda1}${seventeenReplaced}${coda2}\",\"n\"\n";
}

close($fh) or die "Error: problems trying to close file $serials\n";

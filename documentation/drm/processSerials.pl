#!/usr/bin/perl

# This script processes the serial numbers to format them for the Reality 2.0 specification
#
# The expected input is a series of number generated with uuidgen (Mac OS) 
# To flag them as Reality 2.0 number they are prefixed with "R2DS" (Reality 2 for daz Studio)
# To flag them as Reality 3.0 number they are prefixed with "R3PO" (Reality 3 for Poser)

# To hide that they are Generation 4 GUIDs the 12 character, which is always 4, is replaced
# with a randomly chosen character from the word REALITY. 
#
# Finally, the 17th character, one of (A, B, 8, 9), is replaced with a substitution table.
#
# Copyright (c) 2011 Pret-a-3D/Paolo Ciccone. All rights reserved.
#
#

use strict;
use warnings;
use Getopt::Long;

# my $num_args = $#ARGV + 1;
# if ($num_args < 1) {
#   print "\nYou must pass the name of the file containing the serial number, dumbass!\n";
#   exit;
# }

# P=Pret-a-3D, D=DAZ (old, discontinued), R=Renderosity, Y=YURDigital, S=SmithMicro H=Hivewire3D, Z=DAZ
my $vendorCode = "P";  
my %vendorCodes = ( "P" => "Pret-A-3",   "R" => "Renderosity",
                    "D" => "RuntimeDNA",  "Y" => "YURDigital", 
                    "S" => "Smith Micro","H" => "Hivewire",
                    "Z" => "Daz" );

# Prefixes used in the serial number
my %productPrefixes = ( "RE2DS" => "R2DS", 
                        "RE3PO" => "R3PO", 
                        "RE4DS" => "R4DS", 
                        "RE4PS" => "R4PS",
                        "RE4PU" => "R4PU",
                        "RE4DU" => "R4DU");

# Determines if a serial number has been sold, it's "y" for all non-Pret-a-3D numbers
my $inUse = "n";

# How many serial numbers to be generated
my $numSerials = 200;

# The prefix for the serial number
my $prefix  = "R3PO";

# The product code for the database where we store the serial numbers
my $productCode = "RE3PO";

my $dbFile = "serials.txt";
my $vendorFile = "vendorSerials.txt";
my $help = 0;

my $args = GetOptions( "dealer=s" => \$vendorCode,
                       "numSerials=i" => \$numSerials,
                       "productCode=s" => \$productCode,
                       "dbFile=s" => \$dbFile,
                       "vendorFile=s" => \$vendorFile,
                       "help" => \$help
                     );

if ( $help ) {
  print "processSerials.pl --dealer=<code> --numSerial=<number> --productCode=RE2DS|RE3PO|RE4DS|RE4PS\n";
  print "                  --dbFile=<CSV file name> --verdorFile=<Text file name>\n";
  exit;
}

if (!defined($vendorCodes{$vendorCode})) {
  print "The vendor code is not recognized\n";
  exit
}

if (!defined($productPrefixes{$productCode})) {
  print "The product code is not recognized\n";
  exit
}

print "Generating $numSerials serial numbers\n";

my $textSerials = `for i in {1..$numSerials}; do uuidgen; done`;
my @serials = split(/\n/, $textSerials);
print "Vendor code: $vendorCode = ${vendorCodes{$vendorCode}}\n" .
      "Product code: $productCode\n" .
      "Product prefix: ${productPrefixes{$productCode}}\n" .
      "db file: $dbFile\n" .
      "vendor file: $vendorFile\n";

$prefix = $productPrefixes{$productCode};

# for (my $i = 0; $i <= $#serials; $i++) {
#   print "serial #$i: ${serials[$i]}\n";
# }

# If we generate a batch for a distributor then the "in use" column must be "y"
if ($vendorCode ne "P") {
  $inUse = "y";
}

my @replacement1 = ('R','E','A','L','I','T','Y');
my $numReplacements = $#replacement1;
# Replacement tabl for the 17th characters. Another way to obfuscate the 4G GUID
my %seventeenChar = ('A' => 'P', 'B' => 'A', '8' => '3', '9' => 'D');

open(my $DBFILE, ">$dbFile") || die "Cannot open file $dbFile for writing";
open(my $VFILE, ">$vendorFile") || die "Cannot open file $vendorFile for writing";

my $rowNo = 0;
for (my $i = 0; $i <= $#serials; $i++) {
  $_ = $serials[$i];
  # Remove the dashes from the input line
  s/-//g;
  my $line = $_;
  $rowNo++;
  chomp($line);
  # The first 12 characters are OK. The 13th, constant 4, is replaced to hide that this
  # is a th generation GUID
  my $head = substr($line,0,12);
  # Pick a replacement randomly
  my $fourReplaced = $replacement1[rand($numReplacements)];
#  print ">>", substr($line,17,1) , "\n";
  my $seventeenReplaced = $seventeenChar{substr($line,16,1)};
  my $coda1 = substr($line,13,3);
  my $coda2 = substr($line,17);

  # CSV output
  # productCode, serialNumber, seller, used
  # Format of the serial code:
  #   Product prefix: R2DS  (Reality 2 for Daz Studio)
  #   Vendor code
  #   GUID with replaced characters as rules above
  my $serialNo = "${prefix}${vendorCode}${head}${fourReplaced}${coda1}${seventeenReplaced}${coda2}";
  print $DBFILE  "\"$productCode\",\"$serialNo\",\"$inUse\"\n";
  print $VFILE "$serialNo\n";
}

close($DBFILE);
close($VFILE);

## How to Generate the serial numbers for Reality

The serial numbers are obtained by transforming the output of uuidgen. Here are the steps necessary, for example, to generate 2000 numbers for Renderosity.

-   From the shell run this command:

    `./processSerials.pl --dealer=Z --numSerials=300 --productCode=RE3PO --dbFile=DAZ_Reality3PS_serials_formatted.txt --vendorFile=DAZ_Reality3PS_serials_for_DAZ.txt `

    The command creates two files with the 300  numbers. Here is the explanation of the parameters:
    	- --dealer The code for the dealer that manages the serials. Possible choices are: P (Pret-a-3D), R (Renderosity), Y (YURDigital), S (Smith Micro), D (RuntimeDNA, version 3 only), Z (DAZ)
    	- --numSerials The amount of serials to generate
    	- --productCode RE2DS for Reality 2 for DS, RE3PO for Reality 3 for Poser, RE4DS for Reality 4 for DS, RE4PS for Reality 4 for Poser. Note that we move from PO, to indicate Poser, to PS starting with version 4.
    	- --dbFile Name of the CSV file that will contain the serial numbers ready to be importer in the Prêt-à-3D database
    	- --vendorFile Name of the text file that will be given to the vendor to provide serial numbers to the buyers
    	
- Import the dbFile into the table **ProductSerials**.
- Send the vendorFile to the distributor


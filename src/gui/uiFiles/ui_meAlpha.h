/********************************************************************************
** Form generated from reading UI file 'meAlpha.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MEALPHA_H
#define UI_MEALPHA_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "ReTextureAvatar.h"

QT_BEGIN_NAMESPACE

class Ui_meAlphaChannel
{
public:
    QGridLayout *gridLayout;
    ReTextureAvatar *alphaMap;
    QFrame *fr2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_50;
    QDoubleSpinBox *alphaStrength;

    void setupUi(QWidget *meAlphaChannel)
    {
        if (meAlphaChannel->objectName().isEmpty())
            meAlphaChannel->setObjectName(QString::fromUtf8("meAlphaChannel"));
        meAlphaChannel->resize(495, 85);
        QFont font;
        font.setFamily(QString::fromUtf8("Helvetica Neue"));
        meAlphaChannel->setFont(font);
        gridLayout = new QGridLayout(meAlphaChannel);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        alphaMap = new ReTextureAvatar(meAlphaChannel);
        alphaMap->setObjectName(QString::fromUtf8("alphaMap"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(alphaMap->sizePolicy().hasHeightForWidth());
        alphaMap->setSizePolicy(sizePolicy);

        gridLayout->addWidget(alphaMap, 0, 0, 1, 1);

        fr2 = new QFrame(meAlphaChannel);
        fr2->setObjectName(QString::fromUtf8("fr2"));
        sizePolicy.setHeightForWidth(fr2->sizePolicy().hasHeightForWidth());
        fr2->setSizePolicy(sizePolicy);
        verticalLayout_3 = new QVBoxLayout(fr2);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_50 = new QLabel(fr2);
        label_50->setObjectName(QString::fromUtf8("label_50"));

        verticalLayout_3->addWidget(label_50);

        alphaStrength = new QDoubleSpinBox(fr2);
        alphaStrength->setObjectName(QString::fromUtf8("alphaStrength"));
        alphaStrength->setMaximumSize(QSize(60, 16777215));
        alphaStrength->setMaximum(1);
        alphaStrength->setSingleStep(0.1);
        alphaStrength->setValue(1);

        verticalLayout_3->addWidget(alphaStrength);


        gridLayout->addWidget(fr2, 0, 1, 1, 1);


        retranslateUi(meAlphaChannel);

        QMetaObject::connectSlotsByName(meAlphaChannel);
    } // setupUi

    void retranslateUi(QWidget *meAlphaChannel)
    {
        meAlphaChannel->setWindowTitle(QApplication::translate("meAlphaChannel", "Form", 0, QApplication::UnicodeUTF8));
        alphaMap->setCaption(QApplication::translate("meAlphaChannel", "Alpha map", 0, QApplication::UnicodeUTF8));
        alphaMap->setID(QApplication::translate("meAlphaChannel", "alpha", 0, QApplication::UnicodeUTF8));
        alphaMap->setBreadcrumb(QApplication::translate("meAlphaChannel", "alpha", 0, QApplication::UnicodeUTF8));
        label_50->setText(QApplication::translate("meAlphaChannel", "Opacity/Map strength", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class meAlphaChannel: public Ui_meAlphaChannel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MEALPHA_H

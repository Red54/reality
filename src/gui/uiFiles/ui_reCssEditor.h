/********************************************************************************
** Form generated from reading UI file 'reCssEditor.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RECSSEDITOR_H
#define UI_RECSSEDITOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_reCssEditor
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QTextEdit *teCssEditor;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnSave;
    QPushButton *btnClose;
    QLineEdit *searchText;

    void setupUi(QDialog *reCssEditor)
    {
        if (reCssEditor->objectName().isEmpty())
            reCssEditor->setObjectName(QString::fromUtf8("reCssEditor"));
        reCssEditor->resize(1042, 608);
        gridLayout = new QGridLayout(reCssEditor);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(reCssEditor);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(421, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 1, 1, 1, 1);

        teCssEditor = new QTextEdit(reCssEditor);
        teCssEditor->setObjectName(QString::fromUtf8("teCssEditor"));

        gridLayout->addWidget(teCssEditor, 2, 0, 1, 3);

        horizontalSpacer = new QSpacerItem(855, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 3, 0, 1, 2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btnSave = new QPushButton(reCssEditor);
        btnSave->setObjectName(QString::fromUtf8("btnSave"));

        horizontalLayout->addWidget(btnSave);

        btnClose = new QPushButton(reCssEditor);
        btnClose->setObjectName(QString::fromUtf8("btnClose"));

        horizontalLayout->addWidget(btnClose);


        gridLayout->addLayout(horizontalLayout, 3, 2, 1, 1);

        searchText = new QLineEdit(reCssEditor);
        searchText->setObjectName(QString::fromUtf8("searchText"));

        gridLayout->addWidget(searchText, 1, 0, 1, 1);


        retranslateUi(reCssEditor);
        QObject::connect(btnClose, SIGNAL(clicked()), reCssEditor, SLOT(reject()));

        QMetaObject::connectSlotsByName(reCssEditor);
    } // setupUi

    void retranslateUi(QDialog *reCssEditor)
    {
        reCssEditor->setWindowTitle(QApplication::translate("reCssEditor", "Live CSS Editor", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("reCssEditor", "Search", 0, QApplication::UnicodeUTF8));
        btnSave->setText(QApplication::translate("reCssEditor", "Save", 0, QApplication::UnicodeUTF8));
        btnClose->setText(QApplication::translate("reCssEditor", "Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class reCssEditor: public Ui_reCssEditor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RECSSEDITOR_H

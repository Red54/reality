#
# Copyright (c) Pret-a-3D/Paolo Ciccone. All rights reserved.
# Modified by Fuzzy70/Lee Furssedonn with kind permission from Paolo Ciccone
#

from Reality_services import *
from Reality import *

# To customize this script all you need to do is to
# change the following variable
Re_sIBL_Map = ":Runtime:Textures:Reality:SingleIBLs:LineMultiV:LineMultiV_Large_80.ibl"

# Set the IBL Map
Reality.Scene().setIBLImage(ReResolvePoserPath(Re_sIBL_Map).encode("utf8"))
